
public class Main {
    public static void main(String[] args) {
        if (args.length < 4) {
            printHelp();
            return;
        }

        String op = args[0];

        int delimPosition = 0;
        for (int i = 1; i < args.length; i++) {
            if (args[i].equals("--")) {
                delimPosition = i;
                break;
            }
        }
        if (delimPosition == 0) {
            printHelp();
            return;
        }

        int[] coefFirst = parseCoefficients(args, 1, delimPosition);
        int[] coefSecond = parseCoefficients(args, delimPosition + 1, args.length);

        if ((coefFirst.length == 0) || (coefSecond.length == 0)) {
            printHelp();
            return;
        }

        Polynomial first = new Polynomial(coefFirst);
        Polynomial second = new Polynomial(coefSecond);

        Polynomial result;
        String opSymbol;
        if (op.equals("plus")) {
            result = Polynomial.sum(first, second);
            opSymbol = "+";
        } else if (op.equals("times")) {
            result = Polynomial.product(first, second);
            opSymbol = "*";
        } else {
            printHelp();
            return;
        }

        System.out.printf("(%s) %s (%s) = (%s)\n",
                first.toPrettyString("x"),
                opSymbol,
                second.toPrettyString("x"),
                result.toPrettyString("x"));
    }

    private static void printHelp() {
    	System.out.println("Usage: First argument should be an operator, such as: plus(+)  times(*).");
        System.out.println(" First set of input numbers(second set of arguments) are coefficients with descending degrees ( 3 8 6 means 3x^2 + 8x^1 + 6x^0 ");
        System.out.println(" Second set of input numbers(fourth set of arguments) are coefficients with descending degrees ( 3 8 6 means 3x^2 + 8x^1 + 6x^0 ");
        System.out.println("Between the two sets of coefficients should be a separator(--) .");
        System.out.println("No less than 4 sets of arguments should be input.");
        System.out.println("Don't forget about spaces!");
        
        // TODO
    }

    private static int[] parseCoefficients(String[] args, int firstIndexIncl, int lastIndexExcl) {
        int[] result = new int[lastIndexExcl - firstIndexIncl];
        int resIndex = result.length - 1;
        for (int i = firstIndexIncl; i < lastIndexExcl; i++) {
            result[resIndex] = Integer.parseInt(args[i]);
            resIndex--;
        }
        return result;
    }

}
