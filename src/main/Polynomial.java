
/** Integer-only polynomials. */
public class Polynomial {

	/**
	 * Create new instance with given coefficients.
	 *
	 * 1 * x^2 + 3x - 7 would be created by new Polynomial(-7, 3, 1) x^5 + 42 would
	 * be created by new Polynomial(42, 0, 0, 0, 0, 1)
	 *
	 * @param coef Coefficients, ordered from lowest degree (power).
	 */
	int[] coefficients;

	public Polynomial(int... coef) {
		System.out.printf("Creating polynomial");
		for (int i = 0; i < coef.length; i++) {
			if (i > 0) {
				System.out.print(" +");
			}
			System.out.printf(" %d * x^%d", coef[i], i);
		}
		coefficients = coef;
		System.out.println(" ...");
	}

	/** Get coefficient value for given degree (power). */
	public int getCoefficient(int deg) {
		if (deg > coefficients.length - 1) {
			return 0;
		}

		return coefficients[deg];
	}

	/** Get degree (max used power) of the polynomial. */
	public int getDegree() {

		if (coefficients.length < 1) {
			return 0;
		}
		if (coefficients.length == 1) {
			return 1;
		}
		return coefficients.length - 1;
	}

	/** Format into human-readable form with given variable. */
	public String toPrettyString(String variable) {
		String vystup = "";
		boolean first = true;
		for (int i = coefficients.length - 1; i >= 0; i--) {
			if (!first) {
				vystup += " + ";
			}
			if (first) {
				first = false;
			}
			if (getCoefficient(i) == 0) {
				continue;
			}
			else if (getCoefficient (i)==1) {
				if (i == 0) {
				vystup += getCoefficient(i);
				}else if (i == 1) {
					vystup +=   variable + i;
				}else if (i>1) {
					vystup+= variable + "^" + i;
				}
			}
				else if (getCoefficient (i) >1) {
				if (i==0) {vystup+= getCoefficient (i);
			}else if (i==1) { 
				vystup += getCoefficient (i)+ variable;
			}else if (i>0) {	vystup += getCoefficient(i) + variable + "^" + i;
			}
		}
				if (getCoefficient(i) < 0) {
					vystup += " - " + Math.abs(getCoefficient(i)) + variable + "^" + i;
				}

		}
		return vystup;
	}

	/** Debugging output, dump only coefficients. */
	@Override
	public String toString() {
		boolean first = true;
		String vystup = "Polynomial[";
		for (int i = 0; i <= coefficients.length - 1; i++) {
			if (!first) {
				vystup += ",";
			}
			if (first) {
				first = false;

			}
			vystup += getCoefficient(i);
		}

		return vystup + "]";
	}

	/** Adds together given polynomials, returning new one. */
	public static Polynomial sum(Polynomial... polynomials) {
		return new Polynomial(0);
	}

	/** Multiplies together given polynomials, returning new one. */
	public static Polynomial product(Polynomial... polynomials) {
		return new Polynomial(0);
	}

	/** Get the result of division of two polynomials, ignoring remaineder. */
	public static Polynomial div(Polynomial dividend, Polynomial divisor) {
		return new Polynomial(0);
	}

	/** Get the remainder of division of two polynomials. */
	public static Polynomial mod(Polynomial dividend, Polynomial divisor) {
		return new Polynomial(0);
	}
}
